﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcServiceTest;
using System;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            // создаем канал для обмена сообщениями с сервером
            // параметр - адрес сервера gRPC
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            // Http/2 (HTTPS)
            // Http/1
            // Http/1.1 (TSL, SSL) 
            // создаем клиента
            var client = new Greeter.GreeterClient(channel);
            Console.Write("Введите имя: ");
            string name = Console.ReadLine();
            // обмениваемся сообщениями с сервером
            var reply = await client.SayHelloAsync(new HelloRequest { Name = name });
            Console.WriteLine("Ответ сервера: " + reply.Message);
            Console.ReadKey();
        }
    }
}
