﻿using System;
using System.Diagnostics;
using System.IO.Pipes;
using System.Net;
using System.Net.Sockets;

namespace ClientNP
{
    public static class Program
    {
        private static byte[] buffer = new byte[256 * 1024];
        private static Stopwatch watch = new Stopwatch();
        private static long traffic = 0;
        private static int step = 0;

        private static void OnPipeReceive(IAsyncResult ar)
        {
            NamedPipeClientStream client = (NamedPipeClientStream)ar.AsyncState;

            traffic += client.EndRead(ar);
            step++;

            if ((step % 1000) == 0)
            {
                watch.Stop();

                Console.WriteLine(
                  "{0} MB/s",
                  (1000 * (traffic >> 20)) / watch.ElapsedMilliseconds);

                watch.Start();
            }

            client.BeginRead(buffer, 0, buffer.Length, OnPipeReceive, client);
        }

        public static void Main(string[] args)
        {
            NamedPipeClientStream client = new NamedPipeClientStream(".", "TestPipe", PipeDirection.InOut, PipeOptions.Asynchronous);

            client.Connect();

            watch.Start();

            client.BeginRead(buffer, 0, buffer.Length, OnPipeReceive, client);

            Console.WriteLine("CONNECTED");
            Console.ReadLine();
        }
    }
}