﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsLog
{
    interface iEventLogger
    {
        void DebugEntry(string text);
        void WarnEntry(string text);
        void ErrorEntry(string text);
        void AuditFailure(string text);
        void AuditSuccess(string text);
        void ErrorEntry(string text, Exception ex);
    }

    class EventLogger : iEventLogger
    {
        string app;
        public string AppName
        {
            get { return app; }
            set { app = "MySampleAppName"; }
        }
        /// <summary>
        /// Debugging
        /// </summary>
        /// <param name="text"></param>
        public void DebugEntry(string text)
        {
            EventLog.WriteEntry(AppName, text,
               EventLogEntryType.Information);
        }
        /// <summary>
        /// Warning
        /// </summary>
        /// <param name="text"></param>
        public void WarnEntry(string text)
        {
            EventLog.WriteEntry(AppName, text,
               EventLogEntryType.Warning);
        }
        /// <summary>
        /// Error
        /// </summary>
        /// <param name="text"></param>
        public void ErrorEntry(string text)
        {
            EventLog.WriteEntry(AppName, text,
               EventLogEntryType.Error);
        }
        /// <summary>
        /// Audit Failure
        /// </summary>
        /// <param name="text"></param>
        public void AuditFailure(string text)
        {
            EventLog.WriteEntry(AppName, text,
               EventLogEntryType.FailureAudit);
        }
        /// <summary>
        /// Audit Success
        /// </summary>
        /// <param name="text"></param>
        public void AuditSuccess(string text)
        {
            EventLog.WriteEntry(AppName, text,
               EventLogEntryType.SuccessAudit);
        }

        /// <summary>
        /// Error with Exception
        /// </summary>
        /// <param name="text"></param>
        /// <param name="ex"></param>
        public void ErrorEntry(string text, Exception ex)
        {
            EventLog.WriteEntry(AppName, text,
               EventLogEntryType.Error);

        }
    }
}